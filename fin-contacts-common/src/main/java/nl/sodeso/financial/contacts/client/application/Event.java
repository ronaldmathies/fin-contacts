package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.RemoveButton;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.DateField;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.DateType;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Event implements Serializable, IsValidationContainer {

    private boolean removed = false;

    private String uuid = null;
    private DateType event = new DateType();
    private StringType eventTypeValueType = new StringType();
    private OptionType<DefaultOption> eventTypeOptionValue = new OptionType<>();

    private transient EntryForm editForm = null;

    private transient Trigger trigger = null;

    public Event() {
    }

    public EntryForm toEditForm() {
        if (this.editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-event");
        DateField eventDateField = new DateField("event-field", event);

        ComboboxField<DefaultOption> eventTypeField = new ComboboxField<>(eventTypeOptionValue, eventTypeValueType, new DefaultOption("other-option", "other", "Other..."));
        eventTypeField.addItems(
                new DefaultOption("k", "anniversary", "Anniversary"));

        EntryWithWidgetAndWidget entry = new EntryWithWidgetAndWidget("event", eventTypeField, eventDateField);
        entry.setButton(new RemoveButton.WithIcon(clickEvent -> trigger.fire()));
        this.editForm.addEntry(entry);

        return this.editForm;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public DateType getEvent() {
        return event;
    }

    public void setEvent(DateType event) {
        this.event = event;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    public void setRemoveTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public void markRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isMarkedRemoved() {
        return this.removed;
    }

}

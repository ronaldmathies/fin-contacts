package nl.sodeso.financial.contacts.builder;

import nl.sodeso.financial.contacts.client.application.Phonenumber;
import nl.sodeso.financial.contacts.domain.DoPhonenumber;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class PhonenumberBuilder {

    public static void to(List<DoPhonenumber> doPhonenumbers, List<Phonenumber> phonenumbers) {
        Iterator<DoPhonenumber> doPhonenumberIterator = doPhonenumbers.iterator();
        while (doPhonenumberIterator.hasNext()) {
            DoPhonenumber doPhonenumber = doPhonenumberIterator.next();

            for (Phonenumber phonenumber : phonenumbers) {
                if (doPhonenumber.getUuid().equals(phonenumber.getUuid())) {
                    if (phonenumber.isMarkedRemoved()) {
                        doPhonenumberIterator.remove();
                        break;
                    }
                }
            }
        }

        for (Phonenumber phonenumber : phonenumbers) {
            if (phonenumber.isMarkedRemoved()) {
                continue;
            }

            DoPhonenumber doPhonenumberToUpdate = null;
            for (DoPhonenumber doPhonenumber : doPhonenumbers) {
                if (doPhonenumber.getUuid().equals(phonenumber.getUuid())) {
                    doPhonenumberToUpdate = doPhonenumber;
                    break;
                }
            }

            if (doPhonenumberToUpdate != null) {
                to(doPhonenumberToUpdate, phonenumber);
            } else {
                doPhonenumbers.add(to(null, phonenumber));
            }
        }
    }

    public static DoPhonenumber to(DoPhonenumber doPhonenumber, Phonenumber phonenumber) {
        if (doPhonenumber == null) {
            doPhonenumber = new DoPhonenumber();
            doPhonenumber.setUuid(UuidUtils.generate());
        }

        doPhonenumber.setNumber(phonenumber.getPhonenumber());

        if (phonenumber.getLocationOption().getValue() != null) {
            doPhonenumber.setLocation(phonenumber.getLocationOption());
        }
        doPhonenumber.setLocationOther(phonenumber.getLocationOther());

        return doPhonenumber;
    }

    public static ArrayList<Phonenumber> from(List<DoPhonenumber> doPhonenumbers) {
        ArrayList<Phonenumber> phonenumbers = new ArrayList<>();
        for (DoPhonenumber doPhonenumber : doPhonenumbers) {
            phonenumbers.add(from(doPhonenumber));
        }
        return phonenumbers;
    }

    public static Phonenumber from(DoPhonenumber doPhonenumber) {
        Phonenumber phonenumber = new Phonenumber();
        phonenumber.setUuid(doPhonenumber.getUuid());
        phonenumber.setPhonenumber(doPhonenumber.getNumber());
        phonenumber.setLocationOther(doPhonenumber.getLocationOther());

        return phonenumber;
    }

}

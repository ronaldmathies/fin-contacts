package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.Entry;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.AddButton;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class Phonenumbers implements Serializable, IsValidationContainer {

    private ArrayList<Phonenumber> phonenumbers = new ArrayList<>();

    private transient EntryForm editForm = null;
    private transient EntryForm phonenumbersForm = null;

    public Phonenumbers() {}

    public EntryForm toEditForm() {
        if (editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-phonenumbers-form");
        this.phonenumbersForm = new EntryForm("phonenumbers-form");
        this.editForm.addEntry(new EntryWithContainer("", this.phonenumbersForm));

        for (Phonenumber phonenumber : phonenumbers) {
            addPhonenumberToForm(phonenumber);
        }

        final SimpleButton addPhonenumber = new AddButton.WithIcon((event) -> {
            Phonenumber phonenumber = new Phonenumber();
            phonenumbers.add(phonenumber);
            addPhonenumberToForm(phonenumber);
        });

        this.editForm.addEntry(new EntryWithWidgetAndWidget("add-phonenumber-entry", null, null).setButton(addPhonenumber));

        return this.editForm;
    }

    private void addPhonenumberToForm(final Phonenumber phonenumber) {
        EntryForm editForm = phonenumber.toEditForm();

        final EntryWithContainer entry = new EntryWithContainer("phonenumber", editForm);
        phonenumber.setRemoveTrigger(new RemoveEntryTrigger(entry, phonenumber));


        editForm.addRevertableExecutor(() -> {
            if (phonenumber.getUuid() == null) {
                phonenumbers.remove(phonenumber);
                phonenumbersForm.removeEntry(entry);
            } else {
                phonenumber.markRemoved(false);
                entry.setVisible(true);
            }
        });


        phonenumbersForm.addEntry(entry);
    }

    public ArrayList<Phonenumber> getPhonenumbers() {
        return phonenumbers;
    }

    public void setPhonenumbers(ArrayList<Phonenumber> phonenumbers) {
        this.phonenumbers = phonenumbers;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    private class RemoveEntryTrigger implements Trigger {

        private Entry entry = null;
        private Phonenumber phonenumber = null;

        public RemoveEntryTrigger(Entry entry, Phonenumber phonenumber) {
            this.entry = entry;
            this.phonenumber = phonenumber;
        }

        public void fire() {
            if (phonenumber.getUuid() == null) {
                phonenumbers.remove(phonenumber);
                phonenumbersForm.removeEntry(entry);
            } else {
                phonenumber.markRemoved(true);
                entry.setVisible(false);
            }
        }
    }

}

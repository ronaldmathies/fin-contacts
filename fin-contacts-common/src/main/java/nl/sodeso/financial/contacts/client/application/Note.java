package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.input.TextAreaField;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Note implements Serializable, IsValidationContainer {

    private StringType note = new StringType();

    private transient EntryForm editForm = null;

    public Note() {}

    public EntryForm toEditForm() {
        if (this.editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-note");
        TextAreaField noteTextArea = new TextAreaField("note-field", note);
        noteTextArea.setMaximumCharacterCount(1024);

        LegendPanel legendPanel = new LegendPanel("note-panel", "Note(s)");

        EntryForm entryForm = new EntryForm("");
        entryForm.add(new EntryWithSingleWidget("", noteTextArea));

        legendPanel.add(entryForm);

        this.editForm.addEntry(new EntryWithContainer("phonenumber", legendPanel));

        return this.editForm;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    public StringType getNote() {
        return this.note;
    }

    public void setNote(StringType note) {
        this.note = note;
    }
}

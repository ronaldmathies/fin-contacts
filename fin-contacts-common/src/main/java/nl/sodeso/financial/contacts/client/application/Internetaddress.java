package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.RemoveButton;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Internetaddress implements Serializable, IsValidationContainer {

    private boolean removed = false;

    private String uuid = null;
    private StringType internetaddress = new StringType();
    private StringType locationValueType = new StringType();
    private OptionType<DefaultOption> locationOptionValue = new OptionType<>();

    private transient EntryForm editForm = null;
    private transient TextField internetaddressTextField = null;
    private transient Trigger trigger = null;

    public Internetaddress() {
    }

    public EntryForm toEditForm() {
        if (this.editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-internetaddress");
        this.internetaddressTextField = new TextField<>("internetaddress-field", this.internetaddress)
                .addValidationRule(new MandatoryValidationRule("internetaddress-mandatory", ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return internetaddressTextField.getValue();
                    }

                    public boolean isApplicable() {
                        return internetaddressTextField.getValue().isEmpty();
                    }
                });
        this.internetaddressTextField.setPlaceholder("http://www.website.com");

        ComboboxField<DefaultOption> locationField = new ComboboxField<>(locationOptionValue, locationValueType, new DefaultOption("other-option", "other", "Other..."));
        locationField.addItems(
                new DefaultOption("k", "homepage", "Homepage"),
                new DefaultOption("k", "work", "Work"));

        EntryWithWidgetAndWidget entry = new EntryWithWidgetAndWidget("internetaddress", locationField, this.internetaddressTextField);
        entry.setButton(new RemoveButton.WithIcon(clickEvent -> trigger.fire()));

        this.editForm.addEntry(entry);

        return this.editForm;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public StringType getInternetaddress() {
        return internetaddress;
    }

    public void setInternetaddress(StringType internetaddress) {
        this.internetaddress = internetaddress;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    public void setRemoveTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public void markRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isMarkedRemoved() {
        return this.removed;
    }

}

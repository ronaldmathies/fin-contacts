package nl.sodeso.financial.contacts.builder;

import nl.sodeso.financial.contacts.client.application.Communication;
import nl.sodeso.financial.contacts.client.application.Contact;
import nl.sodeso.financial.contacts.domain.DoCommunication;
import nl.sodeso.financial.contacts.domain.DoContact;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ContactBuilder {

    public static DoContact to(DoContact doContact, Contact contact) {
        if (doContact == null) {
            doContact = new DoContact();
            doContact.setUuid(UuidUtils.generate());
        }

        doContact.setFirstname(contact.getFirstname());
        doContact.setLastname(contact.getLastname());
        doContact.setCompany(contact.getCompany());
        doContact.setBirthdate(contact.getBirthdate());
        doContact.setNote(contact.getNote().getNote());

        DoCommunication doCommunication = doContact.getCommunication();
        Communication communication = contact.getCommunication();

        PhonenumberBuilder.to(doCommunication.getPhonenumbers(), communication.getPhonenumbers().getPhonenumbers());
        AddressBuilder.to(doCommunication.getAddresses(), communication.getAddresses().getAddresses());
        EmailaddressBuilder.to(doCommunication.getEmailaddresses(), communication.getEmailaddresses().getEmailaddresses());
        InternetaddressBuilder.to(doCommunication.getInternetaddresses(), communication.getInternetaddresses().getInternetaddresses());
        EventBuilder.to(doContact.getEvents(), contact.getEvents().getEvents());

        return doContact;
    }

    public static ArrayList<Contact> from(List<DoContact> doContacts) {
        ArrayList<Contact> contacts = new ArrayList<>();

        for (DoContact doContact : doContacts) {
            contacts.add(from(doContact));
        }

        return contacts;
    }

    public static Contact from(DoContact doContact) {
        Contact contact = new Contact();
        contact.setUuid(doContact.getUuid());

        contact.setFirstname(doContact.getFirstname());
        contact.setLastname(doContact.getLastname());
        contact.setCompany(doContact.getCompany());
        contact.setBirthdate(doContact.getBirthdate());
        contact.getNote().setNote(doContact.getNote());

        contact.getCommunication().getPhonenumbers().getPhonenumbers().addAll(PhonenumberBuilder.from(doContact.getCommunication().getPhonenumbers()));
        contact.getCommunication().getAddresses().getAddresses().addAll(AddressBuilder.from(doContact.getCommunication().getAddresses()));
        contact.getCommunication().getEmailaddresses().getEmailaddresses().addAll(EmailaddressBuilder.from(doContact.getCommunication().getEmailaddresses()));
        contact.getCommunication().getInternetaddresses().getInternetaddresses().addAll(InternetaddressBuilder.from(doContact.getCommunication().getInternetaddresses()));
        contact.getEvents().getEvents().addAll(EventBuilder.from(doContact.getEvents()));

        return contact;
    }

}

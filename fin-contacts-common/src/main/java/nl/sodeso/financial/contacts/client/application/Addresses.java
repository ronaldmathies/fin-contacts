package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.Entry;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.AddButton;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class Addresses implements Serializable, IsValidationContainer {

    private ArrayList<Address> addresses = new ArrayList<>();

    private transient EntryForm editForm = null;
    private transient EntryForm addressesForm = null;

    public Addresses() {}

    public EntryForm toEditForm() {
        if (editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-addresses-form");
        this.addressesForm = new EntryForm(null);
        this.editForm.addEntry(new EntryWithContainer(null, this.addressesForm));

        for (Address address : addresses) {
            addAddressToForm(address);
        }

        final SimpleButton addAddress = new AddButton.WithIcon((event) -> {
            Address address = new Address();
            addresses.add(address);
            addAddressToForm(address);
        });

        this.editForm.addEntry(new EntryWithWidgetAndWidget("add-address-entry", null, null).setButton(addAddress));

        this.editForm.addRevertableExecutor(() -> addressesForm.setAllEntriesVisible(true));

        return this.editForm;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }

    public ArrayList<Address> getAddresses() {
        return this.addresses;
    }

    private void addAddressToForm(final Address address) {
        EntryForm editForm = address.toEditForm();
        final EntryWithContainer entry = new EntryWithContainer(null, editForm);
        address.setRemoveTrigger(new RemoveEntryTrigger(entry, address));

        editForm.addRevertableExecutor(() -> {
            if (address.getUuid() == null) {
                addresses.remove(address);
                addressesForm.removeEntry(entry);
            } else {
                address.markRemoved(false);
                entry.setVisible(true);
            }
        });

        addressesForm.addEntry(entry);
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    private class RemoveEntryTrigger implements Trigger {

        private Entry entry = null;
        private Address address = null;

        public RemoveEntryTrigger(Entry entry, Address address) {
            this.entry = entry;
            this.address = address;
        }

        public void fire() {
            if (address.getUuid() == null) {
                addresses.remove(address);
                addressesForm.removeEntry(entry);
            } else {
                address.markRemoved(true);
                entry.setVisible(false);
            }
        }
    }
}

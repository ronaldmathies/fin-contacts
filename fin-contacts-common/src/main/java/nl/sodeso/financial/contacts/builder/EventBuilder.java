package nl.sodeso.financial.contacts.builder;

import nl.sodeso.financial.contacts.client.application.Event;
import nl.sodeso.financial.contacts.domain.DoEvent;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class EventBuilder {

    public static void to(List<DoEvent> doEvents, List<Event> events) {
        Iterator<DoEvent> doEventIterator = doEvents.iterator();
        while (doEventIterator.hasNext()) {
            DoEvent doEvent = doEventIterator.next();

            for (Event event : events) {
                if (doEvent.getUuid().equals(event.getUuid())) {
                    if (event.isMarkedRemoved()) {
                        doEventIterator.remove();
                        break;
                    }
                }
            }
        }

        for (Event event : events) {
            if (event.isMarkedRemoved()) {
                continue;
            }

            DoEvent doEventToUpdate = null;
            for (DoEvent doEvent : doEvents) {
                if (doEvent.getUuid().equals(event.getUuid())) {
                    doEventToUpdate = doEvent;
                    break;
                }
            }

            if (doEventToUpdate != null) {
                to(doEventToUpdate, event);
            } else {
                doEvents.add(to(null, event));
            }
        }
    }

    public static DoEvent to(DoEvent doEvent, Event event) {
        if (doEvent == null) {
            doEvent = new DoEvent();
            doEvent.setUuid(UuidUtils.generate());
        }

        doEvent.setEvent(event.getEvent());

        return doEvent;
    }

    public static ArrayList<Event> from(List<DoEvent> doEvents) {
        ArrayList<Event> events = new ArrayList<>();
        for (DoEvent doEvent : doEvents) {
            events.add(from(doEvent));
        }
        return events;
    }

    public static Event from(DoEvent doEvent) {
        Event event = new Event();
        event.setUuid(doEvent.getUuid());
        event.setEvent(doEvent.getEvent());
        return event;
    }

}

package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Communication implements Serializable, IsValidationContainer {

    private Phonenumbers phonenumbers = new Phonenumbers();
    private Emailaddresses emailaddresses = new Emailaddresses();
    private Internetaddresses internetaddresses = new Internetaddresses();
    private Addresses addresses = new Addresses();

    private transient EntryForm editForm = null;
    private transient EntryForm displayForm = null;

    public Communication() {}

    public EntryForm toEditForm() {
        if (this.editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("communication-edit");

        LegendPanel phonenumbersPanel = new LegendPanel("phone-panel", "Phone numbers");
        phonenumbersPanel.add(phonenumbers.toEditForm());
        this.editForm.addEntry(new EntryWithContainer("phone-panel-widget", phonenumbersPanel));

        LegendPanel emailaddressesPanel = new LegendPanel("email-panel", "Email addresses");
        emailaddressesPanel.add(emailaddresses.toEditForm());
        this.editForm.addEntry(new EntryWithContainer("email-panel-widget", emailaddressesPanel));

        LegendPanel internetaddressesPanel = new LegendPanel("url-panel", "Internet Addresses");
        internetaddressesPanel.add(internetaddresses.toEditForm());
        this.editForm.addEntry(new EntryWithContainer("url-panel-widget", internetaddressesPanel));

        LegendPanel addressesPanel = new LegendPanel("url-panel", "Addresses");
        addressesPanel.add(addresses.toEditForm());
        this.editForm.addEntry(new EntryWithContainer("url-panel-widget", addressesPanel));

        return this.editForm;
    }

    public EntryForm toDisplayForm() {
        if (this.displayForm != null) {
            return this.displayForm;
        }

        this.displayForm = new EntryForm("communication-display");

        LegendPanel phonenumbersPanel = new LegendPanel("phone-panel", "Telephone numbers");
        editForm.addEntry(new EntryWithContainer("phone-panel-widget", phonenumbersPanel));

        LegendPanel emailsPanel = new LegendPanel("email-panel", "Email addresses");
        editForm.addEntry(new EntryWithContainer("email-panel-widget", emailsPanel));

        LegendPanel urlsPanel = new LegendPanel("url-panel", "Internet Addresses");
        editForm.addEntry(new EntryWithContainer("url-panel-widget", urlsPanel));

        return this.displayForm;
    }

    public Phonenumbers getPhonenumbers() {
        return this.phonenumbers;
    }

    public Emailaddresses getEmailaddresses() {
        return this.emailaddresses;
    }

    public Internetaddresses getInternetaddresses() {
        return this.internetaddresses;
    }

    public Addresses getAddresses() {
        return this.addresses;
    }

    @Override
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }


}

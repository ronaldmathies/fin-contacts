package nl.sodeso.financial.contacts.builder;

import nl.sodeso.financial.contacts.client.application.Internetaddress;
import nl.sodeso.financial.contacts.domain.DoInternetaddress;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class InternetaddressBuilder {

    public static void to(List<DoInternetaddress> doInternetaddresses, List<Internetaddress> internetaddresses) {
        Iterator<DoInternetaddress> doInternetaddressIterator = doInternetaddresses.iterator();
        while (doInternetaddressIterator.hasNext()) {
            DoInternetaddress doInternetaddress = doInternetaddressIterator.next();

            for (Internetaddress internetaddress : internetaddresses) {
                if (doInternetaddress.getUuid().equals(internetaddress.getUuid())) {
                    if (internetaddress.isMarkedRemoved()) {
                        doInternetaddressIterator.remove();
                        break;
                    }
                }
            }
        }

        for (Internetaddress internetaddress : internetaddresses) {
            if (internetaddress.isMarkedRemoved()) {
                continue;
            }

            DoInternetaddress doInternetaddressToUpdate = null;
            for (DoInternetaddress doInternetaddress : doInternetaddresses) {
                if (doInternetaddress.getUuid().equals(internetaddress.getUuid())) {
                    doInternetaddressToUpdate = doInternetaddress;
                    break;
                }
            }

            if (doInternetaddressToUpdate != null) {
                to(doInternetaddressToUpdate, internetaddress);
            } else {
                doInternetaddresses.add(to(null, internetaddress));
            }
        }
    }

    public static DoInternetaddress to(DoInternetaddress doInternetaddress, Internetaddress internetaddress) {
        if (doInternetaddress == null) {
            doInternetaddress = new DoInternetaddress();
            doInternetaddress.setUuid(UuidUtils.generate());
        }

        doInternetaddress.setInternetaddress(internetaddress.getInternetaddress());

        return doInternetaddress;
    }

    public static ArrayList<Internetaddress> from(List<DoInternetaddress> doInternetaddresses) {
        ArrayList<Internetaddress> internetaddresses = new ArrayList<>();
        for (DoInternetaddress doInternetaddress : doInternetaddresses) {
            internetaddresses.add(from(doInternetaddress));
        }
        return internetaddresses;
    }

    public static Internetaddress from(DoInternetaddress doInternetaddress) {
        Internetaddress internetaddress = new Internetaddress();
        internetaddress.setUuid(doInternetaddress.getUuid());
        internetaddress.setInternetaddress(doInternetaddress.getInternetaddress());
        return internetaddress;
    }

}

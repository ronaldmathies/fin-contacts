package nl.sodeso.financial.contacts.builder;

import nl.sodeso.financial.contacts.client.application.Address;
import nl.sodeso.financial.contacts.domain.DoAddress;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class AddressBuilder {


    public static void to(List<DoAddress> doAddresses, List<Address> addresses) {
        Iterator<DoAddress> doAddressIterator = doAddresses.iterator();
        while (doAddressIterator.hasNext()) {
            DoAddress doAddress = doAddressIterator.next();

            for (Address address : addresses) {
                if (doAddress.getUuid().equals(address.getUuid())) {
                    if (address.isMarkedRemoved()) {
                        doAddressIterator.remove();
                        break;
                    }
                }
            }
        }

        for (Address address : addresses) {
            if (address.isMarkedRemoved()) {
                continue;
            }

            DoAddress doAddressToUpdate = null;
            for (DoAddress doAddress : doAddresses) {
                if (doAddress.getUuid().equals(address.getUuid())) {
                    doAddressToUpdate = doAddress;
                    break;
                }
            }

            if (doAddressToUpdate != null) {
                to(doAddressToUpdate, address);
            } else {
                doAddresses.add(to(null, address));
            }
        }
    }

    public static DoAddress to(DoAddress doAddress, Address address) {
        if (doAddress == null) {
            doAddress = new DoAddress();
            doAddress.setUuid(UuidUtils.generate());
        }

        doAddress.setStreetLine1(address.getStreetLine1());
        doAddress.setStreetLine2(address.getStreetLine2());
        doAddress.setPostalCode(address.getPostalCode());
        doAddress.setCity(address.getCity());

        return doAddress;
    }

    public static ArrayList<Address> from(List<DoAddress> doAddresses) {
        ArrayList<Address> addresses = new ArrayList<>();
        for (DoAddress doAddress : doAddresses) {
            addresses.add(from(doAddress));
        }
        return addresses;
    }

    public static Address from(DoAddress doAddress) {
        Address address = new Address();
        address.setUuid(doAddress.getUuid());
        address.setStreetLine1(doAddress.getStreetLine1());
        address.setStreetLine2(doAddress.getStreetLine2());
        address.setPostalCode(doAddress.getPostalCode());
        address.setCity(doAddress.getCity());

        return address;
    }
}

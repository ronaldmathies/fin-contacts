package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.Entry;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.AddButton;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class Internetaddresses implements Serializable, IsValidationContainer {

    private ArrayList<Internetaddress> internetaddresses = new ArrayList<>();

    private transient EntryForm editForm = null;
    private transient EntryForm internetaddressesForm = null;

    public Internetaddresses() {}

    public EntryForm toEditForm() {
        if (editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-internetaddresses-form");
        this.internetaddressesForm = new EntryForm("internetaddresses-form");
        this.editForm.addEntry(new EntryWithContainer("", this.internetaddressesForm));

        for (Internetaddress internetaddress : internetaddresses) {
            addInternetaddressToForm(internetaddress);
        }

        final SimpleButton addInternetaddress = new AddButton.WithIcon((event) -> {
            Internetaddress internetaddress = new Internetaddress();
            internetaddresses.add(internetaddress);
            addInternetaddressToForm(internetaddress);
        });

        this.editForm.addEntry(new EntryWithWidgetAndWidget("add-internetaddress-entry", null, null).setButton(addInternetaddress));

        return this.editForm;
    }

    private void addInternetaddressToForm(final Internetaddress internetaddress) {
        EntryForm editForm = internetaddress.toEditForm();
        final EntryWithContainer entry = new EntryWithContainer("internetaddress", editForm);
        internetaddress.setRemoveTrigger(new RemoveEntryTrigger(entry, internetaddress));

        editForm.addRevertableExecutor(() -> {
            if (internetaddress.getUuid() == null) {
                internetaddresses.remove(internetaddress);
                internetaddressesForm.removeEntry(entry);
            } else {
                internetaddress.markRemoved(false);
                entry.setVisible(true);
            }
        });

        internetaddressesForm.addEntry(entry);
    }

    public ArrayList<Internetaddress> getInternetaddresses() {
        return internetaddresses;
    }

    public void setInternetaddresses(ArrayList<Internetaddress> internetaddresses) {
        this.internetaddresses = internetaddresses;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    private class RemoveEntryTrigger implements Trigger {

        private Entry entry = null;
        private Internetaddress internetaddress = null;

        public RemoveEntryTrigger(Entry entry, Internetaddress internetaddress) {
            this.entry = entry;
            this.internetaddress = internetaddress;
        }

        public void fire() {
            if (internetaddress.getUuid() == null) {
                internetaddresses.remove(internetaddress);
                internetaddressesForm.removeEntry(entry);
            } else {
                internetaddress.markRemoved(true);
                entry.setVisible(false);
            }
        }
    }
}

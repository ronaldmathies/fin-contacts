package nl.sodeso.financial.contacts.client.application;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.DateField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.DateType;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Contact implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_EDIT_FORM = "contact-edit";
    private static final String KEY_DISPLAY_FORM = "contact-display";

    private String uuid = null;

    private StringType firstname = new StringType();
    private StringType lastname = new StringType();
    private StringType company = new StringType();
    private DateType birthdate = new DateType();

    private Communication communication = new Communication();
    private Events events = new Events();
    private Note note = new Note();

    private transient LegendPanel contactEditPanel = null;
    private transient TextField firstnameTextField = null;
    private transient TextField lastnameTextField = null;
    private transient TextField companyTextField = null;
    private transient DateField birthdateDateField = null;

    public Contact() {}

    public Widget toEditWidget() {
        if (contactEditPanel != null) {
            return contactEditPanel;
        }

        EntryForm editForm = new EntryForm(KEY_EDIT_FORM);

        firstnameTextField = new TextField<>("firstname-field", firstname)
                .addValidationRule(new MandatoryValidationRule("firstname-mandatory", ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return firstnameTextField.getValue();
                    }

                    public boolean isApplicable() {
                        return companyTextField.getValue().isEmpty();
                    }
                });
        editForm.addEntry(new EntryWithLabelAndWidget("firstname", "Firstname", firstnameTextField));

        lastnameTextField = new TextField<>("lastname-field", lastname)
                .addValidationRule(new MandatoryValidationRule("lastname-mandatory", ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return lastnameTextField.getValue();
                    }

                    public boolean isApplicable() {
                        return companyTextField.getValue().isEmpty();
                    }
                });
        editForm.addEntry(new EntryWithLabelAndWidget("lastname", "Lastname", lastnameTextField));

        companyTextField = new TextField<>("company-field", company)
                .addValidationRule(new MandatoryValidationRule("company-mandatory", ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return companyTextField.getValue();
                    }

                    public boolean isApplicable() {
                        return firstnameTextField.getValue().isEmpty() && lastnameTextField.getValue().isEmpty();
                    }
                });
        editForm.addEntry(new EntryWithLabelAndWidget("company", "Company", companyTextField));

        birthdateDateField = new DateField("birthdate-field", birthdate);
        editForm.addEntry(new EntryWithLabelAndWidget("birthdate-entry", "Birthdate", birthdateDateField));

        editForm.addEntry(new EntryWithContainer("communication-entry", communication.toEditForm()));

        LegendPanel eventsPanel = new LegendPanel("events-panel", "Dates");
        eventsPanel.add(events.toEditForm());
        editForm.addEntry(new EntryWithContainer("events-panel-widget", eventsPanel));

        editForm.addEntry(new EntryWithContainer("note-container", note.toEditForm()));

        contactEditPanel = new LegendPanel("contact-panel", "Contact");
        this.contactEditPanel.add(editForm);

        return this.contactEditPanel;
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, contactEditPanel);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(contactEditPanel);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(contactEditPanel);
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public StringType getFirstname() {
        return firstname;
    }

    public void setFirstname(StringType firstname) {
        this.firstname = firstname;
    }

    public StringType getLastname() {
        return lastname;
    }

    public void setLastname(StringType lastname) {
        this.lastname = lastname;
    }

    public StringType getCompany() {
        return company;
    }

    public void setCompany(StringType company) {
        this.company = company;
    }

    public DateType getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(DateType birthdate) {
        this.birthdate = birthdate;
    }

    public Communication getCommunication() {
        return communication;
    }

    public Events getEvents() {
        return events;
    }

    public Note getNote() {
        return note;
    }

    public String getName() {
        StringBuilder name = new StringBuilder();

        if (!lastname.getValue().isEmpty()) {
            name.append(lastname.getValue());
        }

        if (!firstname.getValue().isEmpty()) {
            if (!name.toString().isEmpty()) {
                name.append(", ");
            }
            name.append(firstname.getValue());
        }

        if (name.toString().isEmpty()) {
            if (!company.getValue().isEmpty()) {
                name.append(company.getValue());
            }
        }

        return name.toString();
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Contact) {
            return ((Contact)obj).getUuid().equals(uuid);
        }

        return false;
    }
}

package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.RemoveButton;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Phonenumber implements Serializable, IsValidationContainer {

    private boolean removed = false;

    private String uuid = null;
    private StringType phonenumber = new StringType();
    private StringType locationOther = new StringType();
    private OptionType<DefaultOption> locationOption = new OptionType<>();

    private transient EntryForm editForm = null;
    private transient TextField phonenumberTextField = null;

    private transient Trigger trigger = null;

    public Phonenumber() {
    }

    public EntryForm toEditForm() {
        if (this.editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-phonenumber");
        this.phonenumberTextField = new TextField<>("phonenumber-field", this.phonenumber)
                .addValidationRule(new MandatoryValidationRule("phonenumber-mandatory", ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return phonenumberTextField.getValue();
                    }

                    public boolean isApplicable() {
                        return phonenumberTextField.getValue().isEmpty();
                    }
                });
        phonenumberTextField.setPlaceholder("123 - 456 789");

        ComboboxField<DefaultOption> locationField = new ComboboxField<>(locationOption, locationOther, new DefaultOption("other-option", "other", "Other..."));
        locationField.addItems(
                new DefaultOption("k", "home", "Home"),
                new DefaultOption("k", "work", "Work"),
                new DefaultOption("k", "mobile", "Mobile"),
                new DefaultOption("k", "main", "Main"),
                new DefaultOption("k", "fax-home", "Home Fax"),
                new DefaultOption("k", "fax-home", "Work Fax"),
                new DefaultOption("k", "pager", "Pager"));

        EntryWithWidgetAndWidget entry = new EntryWithWidgetAndWidget("phonenumber", locationField, this.phonenumberTextField);
        entry.setButton(new RemoveButton.WithIcon(clickEvent -> trigger.fire()));

        this.editForm.addEntry(entry);

        return this.editForm;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public StringType getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(StringType phonenumber) {
        this.phonenumber = phonenumber;
    }

    public StringType getLocationOther() {
        return this.locationOther;
    }

    public void setLocationOther(StringType locationOther) {
        this.locationOther = locationOther;
    }

    public OptionType getLocationOption() {
        return locationOption;
    }

    public void setLocationOption(OptionType locationOption) {
        this.locationOption = locationOption;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    public void setRemoveTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public void markRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isMarkedRemoved() {
        return this.removed;
    }
}

package nl.sodeso.financial.contacts.builder;

import nl.sodeso.financial.contacts.client.application.Emailaddress;
import nl.sodeso.financial.contacts.domain.DoEmailaddress;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class EmailaddressBuilder {

    public static void to(List<DoEmailaddress> doEmailaddresses, List<Emailaddress> emailaddresses) {
        Iterator<DoEmailaddress> doEmailaddressIterator = doEmailaddresses.iterator();
        while (doEmailaddressIterator.hasNext()) {
            DoEmailaddress doEmailaddress = doEmailaddressIterator.next();

            for (Emailaddress emailaddress : emailaddresses) {
                if (doEmailaddress.getUuid().equals(emailaddress.getUuid())) {
                    if (emailaddress.isMarkedRemoved()) {
                        doEmailaddressIterator.remove();
                        break;
                    }
                }
            }
        }

        for (Emailaddress emailaddress : emailaddresses) {
            if (emailaddress.isMarkedRemoved()) {
                continue;
            }

            DoEmailaddress doEmailaddressToUpdate = null;
            for (DoEmailaddress doEmailaddress : doEmailaddresses) {
                if (doEmailaddress.getUuid().equals(emailaddress.getUuid())) {
                    doEmailaddressToUpdate = doEmailaddress;
                    break;
                }
            }

            if (doEmailaddressToUpdate != null) {
                to(doEmailaddressToUpdate, emailaddress);
            } else {
                doEmailaddresses.add(to(null, emailaddress));
            }
        }
    }

    public static DoEmailaddress to(DoEmailaddress doEmailaddress, Emailaddress emailaddress) {
        if (doEmailaddress == null) {
            doEmailaddress = new DoEmailaddress();
            doEmailaddress.setUuid(UuidUtils.generate());
        }

        doEmailaddress.setEmailaddress(emailaddress.getEmailaddress());

        return doEmailaddress;
    }

    public static ArrayList<Emailaddress> from(List<DoEmailaddress> doEmailaddresses) {
        ArrayList<Emailaddress> emailaddresses = new ArrayList<>();
        for (DoEmailaddress doEmailaddress : doEmailaddresses) {
            emailaddresses.add(from(doEmailaddress));
        }
        return emailaddresses;
    }

    public static Emailaddress from(DoEmailaddress doEmailaddress) {
        Emailaddress emailaddress = new Emailaddress();
        emailaddress.setUuid(doEmailaddress.getUuid());
        emailaddress.setEmailaddress(doEmailaddress.getEmailaddress());
        return emailaddress;
    }

}

package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.RemoveButton;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Emailaddress implements Serializable, IsValidationContainer {

    private boolean removed = false;

    private String uuid = null;

    private StringType emailaddress = new StringType();
    private StringType locationValueType = new StringType();
    private OptionType<DefaultOption> locationOptionValue = new OptionType<>();

    private transient EntryForm editForm = null;
    private transient TextField emailaddressTextField = null;

    private transient Trigger trigger = null;

    public Emailaddress() {
    }

    public EntryForm toEditForm() {
        if (this.editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-address");
        this.emailaddressTextField = new TextField<>("emailaddress-field", this.emailaddress)
                .addValidationRule(new MandatoryValidationRule("emailaddress-mandatory", ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return emailaddressTextField.getValue();
                    }

                    public boolean isApplicable() {
                        return emailaddressTextField.getValue().isEmpty();
                    }
                });
        this.emailaddressTextField.setPlaceholder("info@website.com");
        ComboboxField<DefaultOption> locationField = new ComboboxField<>(locationOptionValue, locationValueType, new DefaultOption("other-option", "other", "Other..."));
        locationField.addItems(
                new DefaultOption("k", "home", "Home"),
                new DefaultOption("k", "work", "Work"));

        EntryWithWidgetAndWidget entry = new EntryWithWidgetAndWidget("emailaddress", locationField, this.emailaddressTextField);
        entry.setButton(new RemoveButton.WithIcon(clickEvent -> trigger.fire()));

        this.editForm.addEntry(entry);

        return this.editForm;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public StringType getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(StringType emailaddress) {
        this.emailaddress = emailaddress;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.editForm);
    }

    public void setRemoveTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public void markRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isMarkedRemoved() {
        return this.removed;
    }

}

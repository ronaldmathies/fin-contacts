package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.Entry;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.AddButton;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class Emailaddresses implements Serializable, IsValidationContainer {

    private ArrayList<Emailaddress> emailaddresses = new ArrayList<>();

    private transient EntryForm editForm = null;
    private transient EntryForm emailaddressesForm = null;

    public Emailaddresses() {}

    public EntryForm toEditForm() {
        if (editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-emailaddresses-form");
        this.emailaddressesForm = new EntryForm("emailaddresses-form");
        this.editForm.addEntry(new EntryWithContainer("", this.emailaddressesForm));

        for (Emailaddress emailaddress : emailaddresses) {
            addEmailaddressToForm(emailaddress);
        }

        final SimpleButton addEmailaddress = new AddButton.WithIcon((event) -> {
            Emailaddress emailaddress = new Emailaddress();
            emailaddresses.add(emailaddress);
            addEmailaddressToForm(emailaddress);
        });

        this.editForm.addEntry(new EntryWithWidgetAndWidget("add-emailaddress-entry", null, null).setButton(addEmailaddress));

        return this.editForm;
    }

    private void addEmailaddressToForm(final Emailaddress emailaddress) {
        final EntryForm editForm = emailaddress.toEditForm();
        final EntryWithContainer entry = new EntryWithContainer("emailaddresses", editForm);
        emailaddress.setRemoveTrigger(new RemoveEntryTrigger(entry, emailaddress));

        editForm.addRevertableExecutor(() -> {
            if (emailaddress.getUuid() == null) {
                emailaddresses.remove(emailaddress);
                emailaddressesForm.removeEntry(entry);
            } else {
                emailaddress.markRemoved(false);
                entry.setVisible(true);
            }
        });

        emailaddressesForm.addEntry(entry);
    }

    public ArrayList<Emailaddress> getEmailaddresses() {
        return emailaddresses;
    }

    public void setEmailaddresses(ArrayList<Emailaddress> emailaddresses) {
        this.emailaddresses = emailaddresses;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    private class RemoveEntryTrigger implements Trigger {

        private Entry entry = null;
        private Emailaddress emailaddress = null;

        public RemoveEntryTrigger(Entry entry, Emailaddress emailaddress) {
            this.entry = entry;
            this.emailaddress = emailaddress;
        }

        public void fire() {
            if (emailaddress.getUuid() == null) {
                emailaddresses.remove(emailaddress);
                emailaddressesForm.removeEntry(entry);
            } else {
                emailaddress.markRemoved(true);
                entry.setVisible(false);
            }
        }
    }
}

package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.RemoveButton;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Address implements Serializable, IsValidationContainer {

    private boolean removed = false;

    private String uuid = null;
    private StringType streetLine1 = new StringType();
    private StringType streetLine2 = new StringType();
    private StringType postalCode = new StringType();
    private StringType city = new StringType();

    private StringType locationValueType = new StringType();
    private OptionType<DefaultOption> locationOptionValue = new OptionType<>();

    private OptionType<DefaultOption> countryOptionValue = new OptionType<>();

    private transient EntryForm editForm = null;

    private transient Trigger trigger = null;

    public Address() {
    }

    public EntryForm toEditForm() {
        if (this.editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-phonenumber");

        // Type list field and first street line
        TextField streetLine1TextField = new TextField<>("streetLine1-field", this.streetLine1);
        streetLine1TextField.setPlaceholder("Street");
        ComboboxField<DefaultOption> locationField = new ComboboxField<>(locationOptionValue, locationValueType, new DefaultOption("other", "other", "Other..."));
        locationField.addItems(new DefaultOption("home", "home", "Home"), new DefaultOption("work", "work", "Work"));
        EntryWithWidgetAndWidget entry = new EntryWithWidgetAndWidget("streetline1", locationField, streetLine1TextField);
        entry.setButton(new RemoveButton.WithIcon(clickEvent -> trigger.fire()));
        editForm.addEntry(entry);

        // Second street line
        TextField streetLine2TextField = new TextField<>("streetLine2-field", this.streetLine2);
        streetLine2TextField.setPlaceholder("Street");
        editForm.addEntry(new EntryWithLabelAndWidget("streetline2", null, streetLine2TextField));

        // Postalcode
        TextField postcalCodeTextField = new TextField<>("postalcode-field", this.postalCode);
        postcalCodeTextField.setPlaceholder("Postalcode");
        editForm.addEntry(new EntryWithLabelAndWidget("postalcode", null, postcalCodeTextField));

        // City
        TextField cityTextField = new TextField<>("city-field", this.city);
        cityTextField.setPlaceholder("City");
        this.editForm.addEntry(new EntryWithLabelAndWidget("city", null, cityTextField));

        // Country list
        ComboboxField<DefaultOption> countryField = new ComboboxField<>(countryOptionValue);
        countryField.addItems(new DefaultOption("nl", "nl", "Netherlands"), new DefaultOption("de", "germany", "Germany"));
        this.editForm.addEntry(new EntryWithLabelAndWidget("city", null, countryField));

        return this.editForm;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public StringType getStreetLine1() {
        return streetLine1;
    }

    public void setStreetLine1(StringType streetLine1) {
        this.streetLine1 = streetLine1;
    }

    public StringType getStreetLine2() {
        return streetLine2;
    }

    public void setStreetLine2(StringType streetLine2) {
        this.streetLine2 = streetLine2;
    }

    public StringType getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(StringType postalCode) {
        this.postalCode = postalCode;
    }

    public StringType getCity() {
        return city;
    }

    public void setCity(StringType city) {
        this.city = city;
    }

    public void setRemoveTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public void markRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isMarkedRemoved() {
        return this.removed;
    }
}

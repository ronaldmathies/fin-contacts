package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.form.Entry;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.RemoveButton;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class Events implements Serializable, IsValidationContainer {

    private ArrayList<Event> events = new ArrayList<>();

    private transient EntryForm editForm = null;
    private transient EntryForm eventsForm = null;

    public Events() {}

    public EntryForm toEditForm() {
        if (editForm != null) {
            return this.editForm;
        }

        this.editForm = new EntryForm("edit-events-form");
        this.eventsForm = new EntryForm("events-form");
        this.editForm.addEntry(new EntryWithContainer("", this.eventsForm));

        for (Event event : events) {
            addEvent(event);
        }

        final SimpleButton addPhonenumber = new RemoveButton.WithIcon(() -> {
            Event event = new Event();
            events.add(event);
            addEvent(event);
        });

        this.editForm.addEntry(new EntryWithWidgetAndWidget("add-event-entry", null, null).setButton(addPhonenumber));

        return this.editForm;
    }

    private void addEvent(final Event event) {
        EntryForm editForm = event.toEditForm();
        final EntryWithContainer entry = new EntryWithContainer("event", editForm);
        event.setRemoveTrigger(new RemoveEntryTrigger(entry, event));

        editForm.addRevertableExecutor(() -> {
            if (event.getUuid() == null) {
                events.remove(event);
                eventsForm.removeEntry(entry);
            } else {
                event.markRemoved(false);
                entry.setVisible(true);
            }
        });

        eventsForm.addEntry(entry);
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    public ValidationResult validateContainer() {
        return ValidationUtil.validate(editForm);
    }

    private class RemoveEntryTrigger implements Trigger {

        private Entry entry = null;
        private Event event = null;

        public RemoveEntryTrigger(Entry entry, Event event) {
            this.entry = entry;
            this.event = event;
        }

        public void fire() {
            if (event.getUuid() == null) {
                events.remove(event);
                eventsForm.removeEntry(entry);
            } else {
                event.markRemoved(true);
                entry.setVisible(false);
            }
        }
    }

}

package nl.sodeso.financial.contacts.domain;

import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class DoContactFactory {

    private Session session =  ThreadSafeSession.getSession(Constants.PU);

    public List<DoContact> findContacts() {
        Criteria criteria = session.createCriteria(DoContact.class);
        List<DoContact> contacts = criteria.list();
        return contacts;
    }

    public DoContact findContactByUuid(String uuid) {
        Criteria criteria = session.createCriteria(DoContact.class)
                .add(Restrictions.eq("uuid", uuid));
        return (DoContact)criteria.uniqueResult();
    }

    public void saveContact(DoContact doContact) {
        session.save(doContact);
    }

    public void removeContact(String uuid) {
        Criteria criteria = session.createCriteria(DoContact.class)
                .add(Restrictions.eq("uuid", uuid));
        DoContact doContact = (DoContact)criteria.uniqueResult();
        session.delete(doContact);
    }

}

package nl.sodeso.financial.contacts.domain;

import nl.sodeso.gwt.persistence.types.DateTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.DateType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_contact")
@TypeDefs(
        value = {
                @TypeDef(name = "string", typeClass = StringTypeUserType.class),
                @TypeDef(name = "date", typeClass = DateTypeUserType.class)
        }
)
public class DoContact {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid")
    private String uuid;

    @Type(type = "string")
    @Column(name = "firstname", nullable = true, length = 255)
    private StringType firstname;

    @Type(type = "string")
    @Column(name = "lastname", nullable = true, length = 255)
    private StringType lastname;

    @Type(type = "string")
    @Column(name = "company", nullable = true, length = 255)
    private StringType company;

    @Type(type = "column")
    @Column(name = "birthdate", nullable = true)
    private DateType birthdate;

    @Type(type = "string")
    @Column(name = "note", nullable = true, length = 1024)
    private StringType note;

    private DoCommunication doCommunication = new DoCommunication();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DoEvent> doEvents = new ArrayList<>();

    public DoContact() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setFirstname(StringType firstname) {
        this.firstname = firstname;
    }

    public StringType getFirstname() {
        return this.firstname;
    }

    public void setLastname(StringType lastname) {
        this.lastname = lastname;
    }

    public StringType getLastname() {
        return this.lastname;
    }

    public void setCompany(StringType company) {
        this.company = company;
    }

    public StringType getCompany() {
        return this.company;
    }

    public DateType getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(DateType birthdate) {
        this.birthdate = birthdate;
    }

    public StringType getNote() {
        return this.note;
    }

    public void setNote(StringType note) {
        this.note = note;
    }

    public DoCommunication getCommunication() {
        return this.doCommunication;
    }

    public List<DoEvent> getEvents() {
        return this.doEvents;
    }
}

package nl.sodeso.financial.contacts.domain;

/**
 * @author Ronald Mathies
 */
public class Constants {

    /**
     * Persistence unit name.
     */
    public final static String PU = "financialcontacts-pu";

}

package nl.sodeso.financial.contacts.domain;

import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

//import javax.persistence.OrderBy;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Embeddable
public class DoCommunication {

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DoPhonenumber> doPhonenumbers = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DoAddress> doAddresses = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DoEmailaddress> doEmailaddresses = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DoInternetaddress> doInternetaddresses = new ArrayList<>();

    public DoCommunication() {}

    public List<DoPhonenumber> getPhonenumbers() {
        return this.doPhonenumbers;
    }

    public List<DoAddress> getAddresses() {
        return this.doAddresses;
    }

    public List<DoEmailaddress> getEmailaddresses() {
        return this.doEmailaddresses;
    }

    public List<DoInternetaddress> getInternetaddresses() {
        return this.doInternetaddresses;
    }

}

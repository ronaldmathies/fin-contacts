package nl.sodeso.financial.contacts.domain;

import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_emailaddress")
@TypeDefs(
        value = {
                @TypeDef(name = "string", typeClass = StringTypeUserType.class),
                @TypeDef(name = "int", typeClass = IntTypeUserType.class)
        }
)
public class DoEmailaddress implements HasUuid {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "type")
    private Integer type;

    @Type(type = "string")
    @Column(name = "emailaddress", length = 32)
    private StringType emailaddress;

    public DoEmailaddress() {}

    public Long getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public StringType getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(StringType emailaddress) {
        this.emailaddress = emailaddress;
    }
}

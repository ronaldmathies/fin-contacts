package nl.sodeso.financial.contacts.domain;

import nl.sodeso.gwt.persistence.types.DateTypeUserType;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.DateType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_event")
@TypeDefs(
        value = {
                @TypeDef(name = "string", typeClass = StringTypeUserType.class),
                @TypeDef(name = "int", typeClass = IntTypeUserType.class),
                @TypeDef(name = "date", typeClass = DateTypeUserType.class)
        }
)
public class DoEvent implements HasUuid {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid")
    private String uuid;

    @Type(type = "int")
    @Column(name = "type")
    private IntType type;

    @Type(type = "date")
    @Column(name = "event", length = 32)
    private DateType event;

    public DoEvent() {}

    public Long getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public IntType getType() {
        return this.type;
    }

    public void setType(IntType type) {
        this.type = type;
    }

    public DateType getEvent() {
        return event;
    }

    public void setEvent(DateType event) {
        this.event = event;
    }
}

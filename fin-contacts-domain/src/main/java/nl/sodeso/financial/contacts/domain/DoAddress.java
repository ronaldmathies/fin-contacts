package nl.sodeso.financial.contacts.domain;


import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_address")
@TypeDef(name = "string", typeClass = StringTypeUserType.class)
public class DoAddress implements HasUuid {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid")
    private String uuid;

    @Type(type = "string")
    @Column(name = "streetLine1", length = 255)
    private StringType streetLine1 = null;

    @Type(type = "string")
    @Column(name = "streetLine2", length = 255)
    private StringType streetLine2 = null;

    @Type(type = "string")
    @Column(name = "postalCode", length = 255)
    private StringType postalCode = null;

    @Type(type = "string")
    @Column(name = "city", length = 255)
    private StringType city = null;

    public DoAddress() {}

    public Long getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public StringType getStreetLine1() {
        return streetLine1;
    }

    public void setStreetLine1(StringType streetLine1) {
        this.streetLine1 = streetLine1;
    }

    public StringType getStreetLine2() {
        return streetLine2;
    }

    public void setStreetLine2(StringType streetLine2) {
        this.streetLine2 = streetLine2;
    }

    public StringType getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(StringType postalCode) {
        this.postalCode = postalCode;
    }

    public StringType getCity() {
        return city;
    }

    public void setCity(StringType city) {
        this.city = city;
    }
}

package nl.sodeso.financial.contacts.domain;

import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_hone")
@TypeDef(name = "string", typeClass = StringTypeUserType.class)
public class DoPhonenumber implements HasUuid {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid")
    private String uuid;

    @Type(type = "string")
    @Column(name = "location")
    private OptionType location;

    @Type(type = "option")
    @Column(name = "locationOther")
    private StringType locationOther;

    @Type(type = "string")
    @Column(name = "number", length = 32)
    private StringType number;

    public DoPhonenumber() {}

    public Long getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public OptionType getLocation() {
        return location;
    }

    public void setLocation(OptionType location) {
        this.location = location;
    }

    public StringType getLocationOther() {
        return locationOther;
    }

    public void setLocationOther(StringType locationOther) {
        this.locationOther = locationOther;
    }

    public StringType getNumber() {
        return this.number;
    }

    public void setNumber(StringType number) {
        this.number = number;
    }

}

package nl.sodeso.financial.contacts.server.filter;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;
import nl.sodeso.financial.contacts.domain.Constants;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = Constants.PU
)
@FileResources(
        files = {
                @FileResource(file = "/fin-contacts-persistence.properties")
        }
)
public class UnitOfWorkProperties extends FileContainer {
    
}

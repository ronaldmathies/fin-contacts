package nl.sodeso.financial.contacts.client.application;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.TextColumn;
import nl.sodeso.financial.contacts.client.application.rpc.ContactsRpcGateway;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.form.table.DefaultEmptyTableMessage;
import nl.sodeso.gwt.ui.client.form.table.DefaultTableCellGroupHeader;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ContactsTable extends CellTableField<Contact> {

    private SearchCriteria searchCriteria = new SearchCriteria();

    public ContactsTable() {
        super("contacts-table", 10000);

        this.setTableBuilder(new DefaultTableCellGroupHeader<Contact>(this, getListDataProvider()) {
            @Override
            public String getHeaderText(Contact object) {
                return object.getName().substring(0, 1);
            }
        });
        getListDataProvider().addDataDisplay(this);

        // Add a single column for displaying the contacts.
        TextColumn column = new TextColumn<Contact>() {
            @Override
            public String getValue(Contact contact) {
                return contact.getName();
            }
        };
        this.addColumn(column);

        // Create a sort handler.
        ColumnSortEvent.ListHandler<Contact> listHandler = new ColumnSortEvent.ListHandler<>(getListDataProvider().getList());
        listHandler.setComparator(column, new Comparator<Contact>() {
            @Override
            public int compare(Contact o1, Contact o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        this.addColumnSortHandler(listHandler);

        // Set the default sorting to ascending.
        column.setDefaultSortAscending(true);

        // Set the sorting order of the cell list to the first and only column.
        ColumnSortList.ColumnSortInfo columnSortInfo = new ColumnSortList.ColumnSortInfo(column, true);
        this.getColumnSortList().push(columnSortInfo);

        this.setColumnWidth(0, 100, com.google.gwt.dom.client.Style.Unit.PCT);
        this.setEmptyTableWidget(new DefaultEmptyTableMessage("no-contacts", "No Contacts!"));


        refresh();
    }

    public void setContactType(ContactType type) {
        this.searchCriteria.setType(type);
        refresh();
    }

    public void setCriteria(String criteria) {
        this.searchCriteria.setCriteria(criteria);
        refresh();
    }

    public void refresh() {
        ((ContactsRpcGateway) GWT.create(ContactsRpcGateway.class)).findContacts(searchCriteria, new DefaultAsyncCallback<ArrayList<Contact>>() {
            @Override
            public void success(ArrayList<Contact> result) {
                List<Contact> contacts = getListDataProvider().getList();
                contacts.clear();
                contacts.addAll(result);
                ColumnSortEvent.fire(ContactsTable.this, getColumnSortList());
                getSelectionModel().setSelected(contacts.get(0), true);
            }
        });
    }

}

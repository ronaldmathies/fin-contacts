package nl.sodeso.financial.contacts.client.application;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class SearchCriteria implements Serializable{

    private ContactType type;
    private String criteria;

    public SearchCriteria() {
        this.type = ContactType.ALL;
        this.criteria = "";
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public ContactType getType() {
        return this.type;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public String getCriteria() {
        return this.criteria;
    }

}

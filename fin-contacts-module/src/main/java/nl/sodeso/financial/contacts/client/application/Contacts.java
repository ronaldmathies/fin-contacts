package nl.sodeso.financial.contacts.client.application;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.view.client.SingleSelectionModel;
import nl.sodeso.financial.contacts.client.application.rpc.ContactsRpcGateway;
import nl.sodeso.gwt.ui.client.controllers.center.AbstractCenterPanel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.NoButton;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.button.YesButton;
import nl.sodeso.gwt.ui.client.form.input.SearchField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.AttrUtils;
import nl.sodeso.gwt.websocket.client.Message;
import nl.sodeso.gwt.websocket.client.WebSocketClientController;

/**
 * @author Ronald Mathies
 */
public class Contacts extends AbstractCenterPanel {

    private HorizontalPanel horizontalPanel = null;
    private WindowPanel contactPanel = null;

    private ContactsTable contactsTable = null;

    private SearchField searchField = null;
    private StringType seachValue = new StringType();

    private Contact currentContact = new Contact();

    public Contacts() {
        super();

        setFullHeight(true);
    }

    public void beforeAdd(Trigger trigger) {
        if (horizontalPanel == null) {
            horizontalPanel = new HorizontalPanel();
            horizontalPanel.setFullHeight(true);

            searchField = new SearchField("search-contact", seachValue, 3);
            AttrUtils.setFullWidth(searchField);
            searchField.setPlaceholder("Search...");
            seachValue.addValueChangedEventHandler(new ValueChangedEventHandler<StringType>(this) {
                public void onValueCanged(ValueChangedEvent<StringType> event) {
                    contactsTable.setCriteria(event.getValueType().getValue());
                }
            });

            contactsTable = new ContactsTable();
            contactsTable.setFullHeight(true);
            final SingleSelectionModel<Contact> singleSelectionModel = new SingleSelectionModel<>();
            contactsTable.setSelectionModel(singleSelectionModel);
            singleSelectionModel.addSelectionChangeHandler(event -> {
                // If a contact has been selected or is in the process of being
                // created then revert the data.
                if (currentContact != null) {
                    currentContact.revert();
                }

                currentContact = singleSelectionModel.getSelectedObject();
                contactPanel.clearBody();
                contactPanel.addToBody(currentContact.toEditWidget());
            });

            WindowPanel contactsPanel = new WindowPanel("contacts-panel", null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .setBodyPadding(false)
                    .addToBody(contactsTable);

            contactsPanel.addToToolbar(Align.RIGHT, searchField);

            SimpleButton addContact = new SimpleButton("add-contact", "Add Contact", SimpleButton.Style.BLUE);
            addContact.addClickHandler((event) -> addContact());
            contactsPanel.addToFooter(Align.RIGHT, addContact);

            horizontalPanel.addWidget(contactsPanel, 20, Style.Unit.PCT);

            contactPanel = new WindowPanel("contact-panel", null, WindowPanel.Style.INFO)
                    .setFullHeight(true);
            contactPanel.addToBody(currentContact.toEditWidget());

            SimpleButton importContact = new SimpleButton("import-contact", Icon.CloudUpload, SimpleButton.Style.BLUE);
            importContact.addClickHandler((event) -> {
                Message message = new Message();
                message.setCorrelationId(500l);
                WebSocketClientController.instance().send(message);
            });
            SimpleButton exportContact = new SimpleButton("import-contact", Icon.CloudDownload, SimpleButton.Style.BLUE);
            exportContact.addClickHandler((event) -> {
            });
            contactPanel.addToToolbar(Align.RIGHT, importContact, exportContact);

            SimpleButton deleteContact = new SimpleButton("delete-contact", "Delete Contact", SimpleButton.Style.RED);
            deleteContact.addClickHandler((event) -> deleteContact());
            contactPanel.addToFooter(Align.LEFT, deleteContact);

            final SimpleButton saveContact = new SimpleButton("save-contact", "Done", SimpleButton.Style.BLUE);
            saveContact.addClickHandler((event) -> saveContact());
            SimpleButton cancelContact = new SimpleButton("cancel-contact", "Cancel", SimpleButton.Style.RED);
            cancelContact.addClickHandler((event) -> contactPanel.revert());
            contactPanel.addToFooter(Align.RIGHT, saveContact, cancelContact);

            horizontalPanel.addWidget(contactPanel, 80, Style.Unit.PCT);

            add(horizontalPanel);
        }

        trigger.fire();
    }

    public void setContactType(ContactType type) {
        contactsTable.setContactType(type);
    }

    private void saveContact() {
        if (currentContact.hasChanges()) {
            ValidationResult validationResult = ValidationUtil.validate(contactPanel);
            if (ValidationUtil.findWorstLevel(validationResult) == ValidationMessage.Level.INFO) {
                ((ContactsRpcGateway) GWT.create(ContactsRpcGateway.class)).saveContact(currentContact, new DefaultAsyncCallback<Contact>() {
                    @Override
                    public void success(Contact contact) {
                        if (currentContact.getUuid() == null) {
                            contactsTable.add(contact);
                        } else {
                            contactsTable.replace(currentContact, contact);
                        }

                        // Set the result contact as our current contact, the result
                        // contact is different because uuid's are created.
                        currentContact = contact;

                        // Clear the contact panel that now contains the old contact.
                        contactPanel.clearBody();

                        // Add the new contact to the contact panel for editing.
                        contactPanel.addToBody(currentContact.toEditWidget());
                    }
                });
            }
        }
    }

    private void deleteContact() {
        PopupWindowPanel deleteContactPanel = new PopupWindowPanel("delete-contact-popup", "Delete contact permenantly?", WindowPanel.Style.ALERT);
        EntryForm entryForm = new EntryForm("delete-contact-form");
        entryForm.addEntry(new EntryWithDocumentation("delete-contact-message", "Delete contact permenantly?"));
        deleteContactPanel.addToBody(entryForm);

        YesButton.WithLabel yesButton = new YesButton.WithLabel((event) -> {
            deleteContactPanel.close();
            contactsTable.remove(currentContact);
        });

        deleteContactPanel.addToFooter(Align.RIGHT,
                new NoButton.WithLabel((event) -> deleteContactPanel.close()),
                yesButton);
        deleteContactPanel.center(true, true);
    }

    private void addContact() {
        if (currentContact != null) {
            currentContact.revert();
        }

        contactPanel.clearBody();

        currentContact = new Contact();
        contactPanel.addToBody(currentContact.toEditWidget());
    }

}
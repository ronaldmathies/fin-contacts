package nl.sodeso.financial.contacts.server.endpoint.contacts;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.financial.contacts.builder.ContactBuilder;
import nl.sodeso.financial.contacts.client.application.Contact;
import nl.sodeso.financial.contacts.client.application.SearchCriteria;
import nl.sodeso.financial.contacts.client.application.rpc.ContactsRpc;
import nl.sodeso.financial.contacts.domain.DoContact;
import nl.sodeso.financial.contacts.domain.DoContactFactory;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.financialcontacts-contacts")
public class ContactsEndpoint extends XsrfProtectedServiceServlet implements ContactsRpc {

    public ArrayList<Contact> findContacts(SearchCriteria criteria) {
        List<DoContact> contacts = new DoContactFactory().findContacts();
        return ContactBuilder.from(contacts);
    }

    public Contact saveContact(Contact contact) {
        DoContact doContact = null;

        if (contact.getUuid() != null) {
            doContact = new DoContactFactory().findContactByUuid(contact.getUuid());
            ContactBuilder.to(doContact, contact);
        } else {
            doContact = ContactBuilder.to(null, contact);
            new DoContactFactory().saveContact(doContact);
        }

        new DoContactFactory().saveContact(doContact);

        return ContactBuilder.from(doContact);
    }

    public void removeContact(String uuid) {
        new DoContactFactory().removeContact(uuid);
    }

}

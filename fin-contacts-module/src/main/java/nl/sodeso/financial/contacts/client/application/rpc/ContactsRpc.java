package nl.sodeso.financial.contacts.client.application.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.financial.contacts.client.application.Contact;
import nl.sodeso.financial.contacts.client.application.SearchCriteria;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("financialcontacts/endpoint.financialcontacts-contacts")
public interface ContactsRpc extends XsrfProtectedService {

    ArrayList<Contact> findContacts(SearchCriteria criteria);
    Contact saveContact(Contact contact);
    void removeContact(String uuid);

}

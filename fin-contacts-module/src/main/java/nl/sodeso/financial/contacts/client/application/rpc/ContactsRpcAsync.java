package nl.sodeso.financial.contacts.client.application.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.financial.contacts.client.application.Contact;
import nl.sodeso.financial.contacts.client.application.SearchCriteria;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface ContactsRpcAsync {

    public void findContacts(SearchCriteria criteria, AsyncCallback<ArrayList<Contact>> result);
    public void saveContact(Contact contact, AsyncCallback<Contact> result);
    public void removeContact(String uuid, AsyncCallback<Void> result);

}

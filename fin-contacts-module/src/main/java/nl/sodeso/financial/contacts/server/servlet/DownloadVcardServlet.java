package nl.sodeso.financial.contacts.server.servlet;

import nl.sodeso.financial.contacts.domain.DoContact;
import nl.sodeso.financial.contacts.domain.DoContactFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

//import nl.sodeso.contacts.vcard.VcardBuilder2;

/**
 * @author Ronald Mathies
 */
@WebServlet("/financialcontacts/download-vcard")
public class DownloadVcardServlet extends HttpServlet {

    private static final String MIME_TYPE = "text/x-vcard";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uuid = request.getParameter("uuid");
        String vcard = "";

        response.setContentType(MIME_TYPE);

        if (uuid != null) {

            DoContact doContact = new DoContactFactory().findContactByUuid(uuid);
            if (doContact != null) {
//                Ezvcard.write(VcardBuilder.from(doContact)).version(VCardVersion.V4_0).go(response.getOutputStream());
//                String serializedVcard = VcardBuilder2.from(doContact);
//                ServletOutputStream sos = response.getOutputStream();

                response.setStatus(HttpServletResponse.SC_OK);
                OutputStream out = response.getOutputStream();
//                out.write(serializedVcard.getBytes(Charset.forName("UTF-8")));
//                out.flush();
//                response.getWriter().write(serializedVcard);
//                response.getWriter().flush();
            }
        }

//        response.flushBuffer();
    }
}

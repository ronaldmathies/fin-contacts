package nl.sodeso.financial.contacts.server.websocket;

import nl.sodeso.gwt.websocket.client.Message;
import nl.sodeso.gwt.websocket.server.WebSocketServerController;
import nl.sodeso.gwt.websocket.server.WebsocketMessageHandler;

import javax.websocket.Session;

/**
 * @author Ronald Mathies
 */
public class ContactsWebsocketMessageHandler implements WebsocketMessageHandler {

    @Override
    public void onMessage(Session session, Message message) {
        Message response = new Message();
        response.setCorrelationId(1000l);
        WebSocketServerController.instance().send(response);
    }
}

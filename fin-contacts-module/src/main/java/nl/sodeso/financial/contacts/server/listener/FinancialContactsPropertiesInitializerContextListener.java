package nl.sodeso.financial.contacts.server.listener;

import nl.sodeso.financial.contacts.client.Constants;
import nl.sodeso.gwt.ui.server.listener.AbstractPropertiesInitializerContextListener;

import javax.servlet.annotation.WebListener;

/**
 * @author Ronald Mathies
 */
@WebListener
public class FinancialContactsPropertiesInitializerContextListener extends AbstractPropertiesInitializerContextListener {

    @Override
    public Class getServerAppPropertiesClass() {
        return FinancialContactsServerAppProperties.class;
    }

    @Override
    public String getConfiguration() {
        return "fin-contacts-configuration.properties";
    }

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }
}

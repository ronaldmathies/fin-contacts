package nl.sodeso.financial.contacts.client;

import com.google.gwt.core.client.GWT;
import nl.sodeso.financial.contacts.client.application.StartMenu;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.gwt.ui.client.controllers.notification.Notification;
import nl.sodeso.gwt.ui.client.controllers.notification.NotificationController;
import nl.sodeso.gwt.ui.client.controllers.notification.events.CreateNotificationEvent;
import nl.sodeso.gwt.ui.client.link.LinkFactory;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.rpc.DefaultRunAsyncCallback;
import nl.sodeso.gwt.websocket.client.event.WebSocketRecievedEvent;
import nl.sodeso.gwt.websocket.client.event.WebSocketRecievedEventHandler;

/**
 * The application entry point is the main method of a GWT application, it should handle
 * the boodstrapping of the application.
 *
 * @author Ronald Mathies
 */
public class FinancialContactsEntryPoint extends ModuleEntryPoint implements WebSocketRecievedEventHandler {

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    @Override
    public String getModuleName() {
        return "Contacts";
    }

    @Override
    public Icon getModuleIcon() {
        return Icon.Users;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initModule(final InitFinishedTrigger trigger) {
        GWT.runAsync(new DefaultRunAsyncCallback() {

            @Override
            public void success() {
                StartMenu.init();

                if (!LinkFactory.hasLink(getWelcomeToken())) {
                    LinkFactory.addLink(new WelcomeLink());
                }

                trigger.fire();
            }

        });

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getWelcomeToken() {
        return WelcomeLink.TOKEN;
    }

    /**
     * {@inheritDoc}
     */
    public void activateModule(final ActivateFinishedTrigger trigger) {
        GWT.runAsync(new DefaultRunAsyncCallback() {
            @Override
            public void success() {
                trigger.fire();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void suspendModule(final SuspendFinishedTrigger trigger) {
        GWT.runAsync(new DefaultRunAsyncCallback() {

            @Override
            public void success() {
                trigger.fire();
            }

        });
    }

    @Override
    public void onEvent(WebSocketRecievedEvent event) {
        NotificationController.instance().getEventBus().fireEvent(new CreateNotificationEvent(Notification.Style.INFO, "", "" + event.getMessage().getCorrelationId()));
    }

}

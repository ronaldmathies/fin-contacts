package nl.sodeso.financial.contacts.server.filter;

import nl.sodeso.financial.contacts.domain.Constants;
import nl.sodeso.persistence.hibernate.filter.UnitOfWorkFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * @author Ronald Mathies
 */
@WebFilter(
    urlPatterns = {"/*"},
    initParams = {
        @WebInitParam(name = UnitOfWorkFilter.INIT_PERSISTENCE_UNIT, value = Constants.PU),
        @WebInitParam(name = UnitOfWorkFilter.INIT_EXTRA_URL_PATTERN, value = ".*/financialcontacts/.*")
    }
)
public class FinancialContactsUnitOfWorkFilter extends UnitOfWorkFilter {
}

package nl.sodeso.financial.contacts.client.application;

import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItemClickTrigger;

import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class StartMenu {

    private static Contacts contacts = new Contacts();

    private StartMenu() {}

    public static void init() {
        MenuController.instance().setFullwidth(true);

        MenuItem allItems = new MenuItem("all", "All Contacts", new MenuItemClickTrigger() {
            public void click(Map<String, String> arguments) {
                contacts.setContactType(ContactType.ALL);
            }
        });

        MenuController.instance().addMenuItems(
                allItems,
                new MenuItem("people", "People", new MenuItemClickTrigger() {
                    public void click(Map<String, String> arguments) {
                        contacts.setContactType(ContactType.PERSON);
                    }
                }),
                new MenuItem("company", "Companies", new MenuItemClickTrigger() {
                    public void click(Map<String, String> arguments) {
                        contacts.setContactType(ContactType.COMPANY);
                    }
                })
        );

        CenterController.instance().setWidget(contacts);

        allItems.click();
    }

}

package nl.sodeso.financial.contacts.server.endpoint.properties;

import nl.sodeso.financial.contacts.client.Constants;
import nl.sodeso.financial.contacts.client.properties.FinancialContactsClientAppProperties;
import nl.sodeso.financial.contacts.server.listener.FinancialContactsServerAppProperties;
import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractPropertiesEndpoint;
import nl.sodeso.gwt.ui.server.util.ApplicationPropertiesContainer;

import javax.servlet.annotation.WebServlet;

/**
 * By default the application has an endpoint for properties consisting of the following settings:
 *
 * <ul>
 *  <li>name: Name of the application (used as the title)</li>
 *  <li>version: Version number of the application.</li>
 *  <li>devkitEnabled: Are the extra development tools available.</li>
 * </ul>
 *
 * If you would like to have more global application settings (not user specific), that don't
 * change during a session, then this is the place to add them. Just add the fields below
 * and fill them using the endpoint.
 *
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.financialcontacts-properties"})
public class FinancialContactsPropertiesEndpoint extends AbstractPropertiesEndpoint<FinancialContactsClientAppProperties, FinancialContactsServerAppProperties> {

    @Override
    public void fillApplicationProperties(FinancialContactsServerAppProperties serverAppProperties, FinancialContactsClientAppProperties clientAppProperties) {
    }

    @Override
    public Class getClientAppPropertiesClass() {
        return FinancialContactsClientAppProperties.class;
    }

    @Override
    public FinancialContactsServerAppProperties getServerAppProperties() {
        return ApplicationPropertiesContainer.instance().getApplicationProperties(Constants.MODULE_ID);
    }

}


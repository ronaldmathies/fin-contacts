package nl.sodeso.financial.contacts.client.application.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class ContactsRpcGateway implements RpcGateway<ContactsRpcAsync>, ContactsRpcAsync {

    /**
     * {@inheritDoc}
     */
    public ContactsRpcAsync getRpcAsync() {
        return GWT.create(ContactsRpc.class);
    }
}

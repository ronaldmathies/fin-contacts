package nl.sodeso.financial.contacts.client.application;

/**
 * Created by sodeso on 09/03/15.
 */
public enum ContactType {

    ALL,
    PERSON,
    COMPANY

}

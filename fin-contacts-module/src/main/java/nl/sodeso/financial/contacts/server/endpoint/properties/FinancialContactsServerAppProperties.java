package nl.sodeso.financial.contacts.server.endpoint.properties;

import nl.sodeso.gwt.ui.server.util.ServerAppProperties;

/**
 * @author Ronald Mathies
 */
public class FinancialContactsServerAppProperties extends ServerAppProperties {

    public FinancialContactsServerAppProperties(String configurationFile) {
        super(configurationFile);
    }
}
